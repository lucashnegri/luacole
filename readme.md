# luacole - Cole-Cole parameter extraction for/in Lua

## About

*luacole* uses a differential evolution algorithm to determine what are the
parameters of the Cole-Cole model that better describes a target impedance
spectrum. It is also possibile to import *core.lua* to use it as a library too
(no namespace, but you can handle that!).

## Sample usage

    ** Solution **
    |----------------
    |Iterations: 285  
    |Fitness   : 3.72e-10
    |----------------
    | Rinf  = 45   
    | R0    = 200   
    | Tau   = 2e-07   
    | Alpha = 0.75   
    | Lag   = 1e-08   
    |----------------

Note that the files *modulus.svg, nyquist.svg, phase.svg* are created /
overwriten after execution. To avoid the output of the plots, change *doplot*
to *noplot*.

## License

GPLv3+.
