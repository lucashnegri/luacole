#! /usr/bin/env luajit

--[[
    Generates a spectrum based on the Cole-Cole parameters.
    Usage: manually or with ./gen < input > output .
--]]

require('core')

local function read_param(name)
    io.stderr:write(string.format("%s = ", name))
    return io.read('*n')
end

io.stderr:write("** Model parameters **\n")
local params = {
    read_param( pnames[1] ),
    read_param( pnames[2] ),
    read_param( pnames[3] ),
    read_param( pnames[4] ),
    read_param( pnames[5] ),
}

io.stderr:write("** Frequency range **\n")
local from = read_param("From [Hz]")
local to   = read_param("To   [Hz]")
local ppd  = read_param("p.p.d    ")

local gen  = generate(params, from, to, ppd)

for i = 1, #gen.freq do
    print(gen.freq[i], gen.imp[i]:real(), gen.imp[i]:imag())
end
