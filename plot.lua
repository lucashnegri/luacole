local gp = require('gnuplot')

function plotit(data, best)
    local fit = generate(best, 1e1, 1e7, 30)
    
    -- plot the modulus
    gp{
        width    = 400,
        height   = 250,
        xlabel   = "Frequency [Hz]",
        xformat  = "10^%L",
        ylabel   = "Impedance [{/Symbol W}]",
        logscale = "x",
        key      = "left bottom",
        
        data = {
            gp.array{
                {
                    data.freq,
                    map(data.imp, complex.abs)
                },
                title = "Original data",
                width = 4,
            },
            gp.array{
                {
                    fit.freq,
                    map(fit.imp, complex.abs)
                },
                title = "Fitted data",
                width = 4,
            },
        }
    }:plot("modulus.svg")
    
    -- plot the phase
    gp{
        width    = 400,
        height   = 250,
        xlabel   = "Frequency [Hz]",
        xformat  = "10^%L",
        ylabel   = "Phase [rad]",
        logscale = "x",
        key      = "left bottom",
        
        data = {
            gp.array{
                {
                    data.freq,
                    map(data.imp, complex.arg)
                },
                title = "Original data",
                width = 4,
            },
            gp.array{
                {
                    fit.freq,
                    map(fit.imp, complex.arg)
                },
                title = "Fitted data",
                width = 4,
            },
        }
    }:plot("phase.svg")
    
    -- Nyquist plot
    gp{
        width    = 400,
        height   = 300,
        xlabel   = "-Reactance [{/Symbol W}]",
        ylabel   = "Resistance [{/Symbol W}]",
        key      = "center bottom",
        
        data = {
            gp.array{
                {
                    map(data.imp, complex.real),
                    map(map(data.imp, complex.conj), complex.imag)
                },
                width = 4,
                title = "Original data",
            },
            gp.array{
                {
                    map(fit.imp, complex.real),
                    map(map(fit.imp, complex.conj), complex.imag)
                },
                width = 4,
                title = "Fitted data",
            }
        }
    }:plot("nyquist.svg")
end
