local de  = require('de')
local res = pcall(require, 'fcomplex')
if not res then require('complex') end

pnames = {
    "Rinf ", "R0   ", "Tau  ", "Alpha", "Lag  "
}

-- read the parameters and complex impedance spectra from a file
function read_data(arq)
    local f = io.open(arq)
    local p = {}
    
    p[1] = {f:read('*n'), f:read('*n')} -- RInf
    p[2] = {f:read('*n'), f:read('*n')} -- R0
    p[3] = {f:read('*n'), f:read('*n')} -- Tau
    p[4] = {f:read('*n'), f:read('*n')} -- Alpha
    p[5] = {f:read('*n'), f:read('*n')} -- Parasite capacitance
    
    local n    = f:read('*n')
    local data = { freq = {}, imp = {} }
    
    for i = 1, n do
        local freq       = f:read('*n')
        local real, imag = f:read('*n'), f:read('*n')
        assert(freq and real and imag, 'Invalid input file')
        table.insert( data.freq, freq )
        table.insert( data.imp , real + complex.I*imag )
    end
    
    f:close()
    
    return p, data
end

-- cole-cole model with additional terms
function cole(freq, v)
    local omega = 2*math.pi*freq
    local num   = v[2] - v[1]
    local den   = 1 + (complex.I*omega*v[3])^v[4]
    local Z     = v[1] + num / den
    
    if v[5] ~= 0 then
        Z = Z * complex.exp(-omega * complex.I*v[5]);
    end
    
    return Z
end

function map(t, f)
    local o = {}
    for i in pairs(t) do o[i] = f(t[i]) end
    return o
end

function next_decade(v)
    return math.pow(10, math.floor(math.log10(v))+1)
end

-- fitness evaluation
function fitness(v, data)
    local acc = 0
    local freq, imp = data.freq, data.imp
    
    for i = 1, #freq do
        local out = cole(freq[i], v)
        local err = complex.abs(out - imp[i])
        acc = acc + err*err
    end
    
    return -acc / #freq
end

function generate(params, from, to, ppd)
    local gen = {freq = {}, imp = {}}

    local freq = from;
    while freq <= to do
        local imp = cole(freq, params)
        table.insert(gen.freq, freq)
        table.insert(gen.imp , imp)
        freq = freq + next_decade(freq) / ppd
    end
    
    return gen
end

function fit(params, data)
    -- create and configure the solver
    local solver   = de.new(5, 20)
    solver.limits  = params
    solver.ud      = data
    solver.fitness = fitness
    solver:init()

    -- Run!
    local it, fv, best = solver:run(1000, 1e-6, 100)
    return it, fv, best
end
