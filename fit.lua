#! /usr/bin/env luajit

--[[
    Extracts the Cole-Cole parameters by using differential evolution.
--]]

require('core')

-- ** Main **
math.randomseed( os.time() )

-- read the input file
local params, data = read_data(arg[1])
local doplot = arg[2] ~= 'noplot'

-- fit
local it, fv, best = fit(params, data)

print(string.format([[
** Solution **
|----------------
|Iterations: %d  
|Fitness   : %.3g
|----------------
| %s = %.3g   
| %s = %.3g   
| %s = %.3g   
| %s = %.3g   
| %s = %.3g   
|----------------]], it, -fv,
    pnames[1], best[1], pnames[2], best[2], pnames[3], best[3],
    pnames[4], best[4], pnames[5], best[5]
))

-- Plot some nice figures
if doplot then
    require('plot')
    plotit(data, best)
end
